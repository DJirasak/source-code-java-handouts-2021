public class Ex2_Trivial_printing_samples {
    public static void main(String[] args) {
        System.out.println("");
        System.out.println("         X");
        System.out.println("        * *");
        System.out.println("       *   *");
        System.out.println("      *  o  *");
        System.out.println("     *     v *");
        System.out.println("    * v       *");
        System.out.println("   *        o  *");
        System.out.println("    ***********");
        System.out.println("     ___|_|___");
    }
}
